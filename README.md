# SignalR/Redis Kubernetes working example

The intent of this project is to provide a built-from-scratch working example that illustrates some key K8S advantages and best practices when it comes to development:

- A real local development option including code-reloading
- How to get started with Helm
- An actual, working (if simple) multi-tier service
- The usage of K8S `Operator` tools
- Automated, integrated telemetry and logging

This was built on top of a `microk8s` running locally on a Mac inside of a 4GB `multipass` VM. On Linux, no VM should be necessary, and it needs to be **TESTED** on Windows.

There's no reason this couldn't also work with `minikube`, or [TODO], with a remote dev environment on a cloud provider.

The actual deployed service is a two-tier web chat app using a basic .NET Core SignalR as the application server with multiple instances syncrhonizing using a Redis backend.

## Getting MicroK8S installed and working


MicroK8S also packages some commonly used infrastructure services as addons (e.g., Istio, Prometheus, Kuberntes Dashboard). For this example, four of these will need to be enabled:

- DNS
- Prometheus
- Registry
- MetalLB

The first three require no configuration and can be enabled with a simple `microk8s enable dns prometheus registry` command. MetalLB can require configuration if MicroK8S isn't running on Linux, because it will be in a separate VM that is in turn connected to a virtual network. 

On a Mac, for instance, once the VM is up and running, the K8S API IP can be found with this command:

```bash
microk8s config | egrep server
```

This should return something that looks much like:

```
server: https://192.168.64.4:16443
```

The IP address is what is really needed. The host will typically be bridged into the virtual network (default on Mac), so it's safe to assume that the `192.168.64.0/24` is the network being used. To confirm, the output of the `ifconfig` command on a Mac will show more details, typically under the `bridge100` interface. 

This is all to say that it's typically safe to pick a range of addresses in this subnet and tell MetalLB to hand them out to K8S Services that request a type of `LoadBalancer`. In this example, MetalLB was enabled and configured with this command:

```bash
microk8s enable metallb:192.168.64.50-192.168.64.100
```

...which gives MetalLB 50 IP addresses to hand out, more than enough for most local development scenarios.

## Installing `kubectl` and `helm`

Two CLI tools that every K8S admin will need are `kubectl` and `helm`. `kubectl` allows for CLI-based interaction with the Kubernetes API, similar to the `aws` or `azure` CLI tools. 

## Installing and configuring Skaffold

[Skaffold](https://skaffold.dev) is what's often referred to as an "inner loop" development helper for Kubernetes. Rather than endlessly doing a `docker build` and `docker run` and then using `docker-compose` or manually deploying using `helm` or `kubectl` once the container is built, `skaffold` watches your local directory and when the contents of the code, the Dockerfile or the K8S manifest templates change, it re-deploys them automatically.

There are alternatives to Skaffold that perform the same function (Draft, Garden.IO, Tilt.dev) that any team should evaluate as part of creating a standard workflow for Kubernetes. In this case, Skaffold was chosen because it's reasonably mature, supported by Google, and supports multiple workflow patterns (e.g., local machine for devs with workstations that can support it, remote deployment to a cloud provider or local cluster for those that don't). 

This isn't intended to read as an endorsement of Skaffold over the other options, it **is** a recommendation that using *any* of them is better than not having any tooling at all.

On a Mac, it's easy to install with a `brew install skaffold`, the documentation linked about should cover Linux and Windows installs.

## The initial SignalRChat code

What this isn't intended to be is a .NET Core/Signal R tutorial, so the code is complete and working as-is. It is based on a [publicly available example from Microsoft](https://docs.microsoft.com/en-us/aspnet/core/tutorials/signalr?view=aspnetcore-5.0&tabs=visual-studio-code).

If the `dotnet` environment is on the local machine, it can be run locally by changing to the `SignalRChat` directory and running a `dotnet run -p SignalRChat.csproj`.

One advantage of using a container-based workflow for locally development is that it's not *neccesary* to have/keep updated a complicated runtime like .NET Core.

**NOTE:** This code includes an `appsettings.json` file that has been changed from the default (which will tell the app to listen on the default HTTP port of `80`). Because of how Kubernetes works, there's no need to run the app on the default HTTP port, or really *any* specific port (the Kubernetes Service will handle this). There is further explanation of why this was done down in the security section. If this pattern is re-used, just remember that the container will need to listen a port > 1024.

## The Dockerfile

In order to build a container for any application, it needs to have a `Dockerfile`. This project isn't intended to be a full tutorial on writing Dockerfiles, though it does contain some best practices and a couple of patterns specific to .NET Core that will be explained in some detail.

The first line of any `Dockerfile` is the `FROM` command. This specifies a base Docker image to use during the build. For .NET Core projects, the base image to use for the build step is typically `mcr.microsoft.com/dotnet/sdk`, so the actual line reads:

```docker
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
```

Notice the version specification trailing the container name as `:5.0`. If no version is specified, Docker will imply the `latest` tag, which tracks whatever the most recent release is, regardless of whether it's a major or minor version upgrade, so whatever version of the runtime the app is developed against needs to be specified in the `Dockerfile`. Also note that testing a version upgrade (e.g. `5.1`) would start with bumping the version number in the `Dockerfile` and seeing what happens, making it much easier to do than having to update locally.

The `AS build` at the end of the line is also important. For performance and security reasons, deployed Docker containers should be as small as possible and contain only what is need to run the deployed artifact. To make this process easier, Docker supports what are called multi-stage builds. So the first section of the `Dockerfile` will use the .NET SDK image (which is quite large and contains a rich set of development tools), but the second stage will take the built artifact (`SignalRChat.dll`) and copy it into a stripped-down ASP.NET container that only has the minimal set of binaries to run the application itself.

The next three lines look like this:

```docker
WORKDIR /app
COPY *.csproj ./
RUN dotnet restore
```

This first change the directory inside the container to `/app` (creating it at the same time), then copies the `SignalRChat.csproj` file to it. Finally, it runs a `dotnet restore`.

The reason this is done rather than simply copying the entirety of the folder with the code over is to take advantage of how Docker builds images using layers (which are just virtual filsystems that get packaged up together during the build). Each line in the Dockerfile will map onto a layer (which Docker keeps track of on our behalf). If none of the files on the local filesystem that map onto a layer have changed (determined by the `COPY` command), Docker will use it's cached version. For doing something like a `dotnet restore`, which restores all of the dependencies, this can save a lot of time during each build. For JavaScript containers, this same pattern is typically followed by copying over the `package.js` and `package-lock.js` files in one step, then running `npm install`, *then* copying over the rest of the JS source and building it.

The actual build for a .NET project looks like this:

```docker
COPY . ./
RUN dotnet publish -c Release -o out
```

This is pretty straightforward. Copy over the remainder of the source files and build the application.

Now onto building the actual image to be deployed. It starts with another `FROM` line, this time specifying the simple ASP.NET runtime image rather than the full SDK:

```docker
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app/out .
EXPOSE 8080
ENTRYPOINT ["dotnet", "SignalRChat.dll"]
```

The third line is a `COPY`, but instead of copying from the local filesystem, it copies from the `build` stage image. Again, this pattern of building in one or more stages and then copying over just the built artifacts to a final minimal container is good to keep in mind, both for speeding up the local development cycle, but also for building better, more secure containers.

## Skaffold setup

Now that a Dockerfile exists, the final piece are the initial Kubernetes deployment manifests.

This example will work through these progressively, initially just getting a K8S Deployment of the container up and running with a single instance.

Skaffold can be used to generate an initial Deployment and Service manifest, but in most cases it's easier to simply start from scratch. Create a new file in the directory called `signal-r-chat-deployment.yaml`, with this content:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: signal-r-chat
  labels:
    app: signal-r-chat
spec:
  replicas: 1
  selector:
    matchLabels:
      app: signal-r-chat
  template:
    metadata:
      labels:
        app: signal-r-chat
    spec:
      containers:
      - name: signal-r-chat
        image: localhost:32000/signal-r-chat
        ports:
          - containerPort: 8080
```

Again, this example will not attempt to explain every line, the Kubernetes documentation is quite good, but the key points are:

- A Deployment means that Kubernetes will always try to keep the specified number of `replicas` running. It's possible to declare a `Pod`, and many "hello world" examples do, but there's little use case for them in a production system. A Deployment also allows for rolling updates (e.g., if three replicas were running, one new one would be started, K8S would wait for it to pass readiness checks, then shut down one of the old containers and proceed onto a second one). Later on in the example, the manifest for the Deployment will be changed to illustrate how this works, but for now it's just getting one container running.
- The `image` block refers to `localhost:32000` because Skaffold will be configured to build and then push the image to container registry inside of MicroK8S. 
- The `ports` block is used to specify which port(s) the container will listen on. A Deployment doesn't expose any ports outside of the cluster, so this is simply about specifying which ports are even available to be exposed at some point in the future.

Once this file is saved, there is enough configuration available to do a `skaffold init`. The CLI in this case will give the choice to either use the Dockerfile or the `SignalRChat.csproj` file as the 'builder', choose the Dockerfile, then confirm that the `skaffold.yaml` file should be written.

Skaffold should print a message into the console saying that it can now run in dev or deploy modes.

The `skaffold.yaml` file also specifies *which* YAML files should actually be deployed under the `deploy` block in the `manifests` section. The auto-generated config file will only refer to the initial `signal-r-chat-deployment.yaml` file, but the intent is to add other resources, so replace the `deployment` with a `*` so it will be ready for that.

A final adjustment is needed due to using the local registry. On Linux, the `localhost:32000` value should work fine, but on a Windows or Mac development workstation the IP address of the Kuberntes API will be needed. As show up above, a `microk8s status | egrep server` command can be used to determine this. In the `skaffold.yaml` file, change the `- image:` value under `artifacts` to `<IP address of server>:32000`. The reason for this is that the build runs locally, but once the container is pushed, the actual manifest gets run on the MicroK8S cluster, for which the registry is `localhost:32000`.

At this point, it should be possible to run a `skaffold dev` and watch if first build the container, push it, then apply the Deployment to the MicroK8S cluster. If it's worked, a 'Watching for changes...' will print at the bottom of the terminal.

While this is running, any changes to the repo will result in an automatic update to the cluster, which will get demonstrated shortly when a Service definition is added.

As an aside, it's possible to connect directly to a running Pod using the `kubectl port-forward` command. So, even though the current Deployment has no externally exposed ports, it could be tested by running a `kubectl port-forward <signal-r-chart-pod-name> 8080:80` in a terminal window, and then opening a browser window to `http://localhost:8080`. If it's worked, a basic `SignalRChat` interface will come up.

## Adding a Service

If the deployed Pod is reachable with the correct `kubectl port-forward` command, then the next step is to expose it with a Service. There are a few different types of [Service in Kubernetes](https://kubernetes.io/docs/concepts/services-networking/service/), and again, refer to the linked documentation for details, but the key point is that a Service selects Pods using labels and exposes them in a consistent manner regardless of what happens to the Pods behind it (e.g., one dies and is replaced by K8S or the Deployment is scaled up to handle load). Services also interact with Kuberentes' DNS service so they can be discovered using a predictable naming convention and greatly simplifying the process of getting services communicating with each other.

Create another file in the directory called `signal-r-chat-service.yaml` and make it look like this:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: signal-r-chat-service
spec:
  type: LoadBalancer
  selector:
    app: signal-r-chat
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
```

The intent here should be relatively clear. A service should be created to expose Pods with a label of `app: signal-r-chat` (note that the `selector:` block can be significantly more complex), and it will expose it on a load balancer at port 80, which is mapped to port 80 inside the Pod(s). Note that the `targePort` argument should match the `containerPort` argument in the Deployment manifest. Also note that Services can expose more than one port (e.g., 80 and 443 for HTTP and HTTPS traffic).

When the file gets saved, the terminal window that `skaffold dev` is running in should automatically update the configuration, and a `kubectl get services` should show something like this:

```
NAME                    TYPE           CLUSTER-IP       EXTERNAL-IP     PORT(S)        AGE
kubernetes              ClusterIP      10.152.183.1     <none>          443/TCP        9d
signal-r-chat-service   LoadBalancer   10.152.183.199   192.168.64.51   8080:31934/TCP   73s
```

The `EXTERNAL-IP` field shows the IP address to hit, try to open it in a browser window to make sure things are working as intended.

## K8S Best Practice: [Resource Requests and Limits](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/)

One somewhat under-appreciated capability Kubernetes provides is that if a Deployment is correctly configured with resource limits, it can deal with memory leaks. Obviously, if a leak grows quickly enough the container(s) will always be restarting, but for something more occasionaly, when the Pod hits configured memory limits, Kubernetes will kill it and start another. This is also true for CPU limits, if for some reason a process gets stuck with the CPU pinned, Kubernetes can do the same thing.

Resource requests are related, but used for a different purpose. The requested resources are essentially a guide to the Kubernetes scheduler that helps it make sure a Pod is being deployed onto a node with adequate resources.

**Both** of these should be required to be configured on any deployment past a `dev` environment. There are methods to do policy enforcement like this, it could be as simple as a `grep` in a CI/CD stage, or as complicated as writing a [Kubernetes Admission Controller](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/) that enforces good behavior at the API level.

Though this project is only currently getting deployed to `dev`, some resource requests and limits can be added as an example. Note that ideally this can be handled with Helm, which, by default, will generate templates that allow these values to be configured or not depending on which environment the deployment specifies.

Edit the `signal-r-chat-deployment.yaml` file and modify the `spec` block to make it look like it does below, then save it, Skaffold will automatically update it if it's still running:

```yaml
    spec:
      containers:
      - name: signal-r-chat
        image: localhost:32000/signal-r-chat
        resources:
          requests:
            memory: "32Mi"
            cpu: "100m"
          limits:
            memory: "48Mi"
            cpu: "200m"
        ports:
          - containerPort: 8080

```

Determining good starting values for these is somewhat out-of-scope for this document, though it will cover gather telemetry data later, which can be used to inform starting values. Obviously, for production use, a certain amount of load testing would be required.  

## K8S Best Practice: Don't run the app as the root user

By default, processes started inside a Docker container (which really should only be one) are started as the `root` user. This means if for some reason the container was compromised, the user would be able to do whatever they wanted inside of it.

Few services need to run as root, and a combination of Docker commands in the `Dockerfile` and parameters in the K8S manifests can essentially close this door. It's also good practice to not install `sudo` or `gosu` inside containers unless it's absolutely necessary for this reason.

In the Dockerfile, change the second stage to look like this:

```docker
# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app/out .
# Add a non-privileged user to run the app as
RUN useradd --home-dir /app --no-create-home \
    --no-log-init --user-group --uid 1000 signalrchat && \
    chown -R signalrchat:signalrchat ./* && \
    chmod 500 ./*
USER signalrchat
ENTRYPOINT ["dotnet", "SignalRChat.dll"]
```

A couple of notes and explanation on this:

- The user's home directory is set to `/app`, where the build artifacts were copied
- The `--no-create-home` is necessary to prevent the existing directory being squashed
- The `--no-log-init` is a workaround for a bug in Go that effects Docker
- The `--user-group` creates a group with the same name, which is useful for this case
- The `--uid` explicitly specifies the UID, which is good practice because it will be specified by the Kubernetes manifest
- The contents of the `/app` directory are then changed to be owned by `signalrchat`
- The contents of the `/app` directory are changed so only their own can read or execute them
- Finally, the `USER` command in Docker specifies which user to run as

The container runtime will honor the `USER` directive, but changes should also be made to the Kubernetes manifest.

In the `signal-r-chat-deployment.yaml` file, under the `containers:` block, make it look like this:

```yaml
      containers:
      - name: signal-r-chat
        image: localhost:32000/signal-r-chat
        resources:
          requests:
            memory: "32Mi"
            cpu: "100m"
          limits:
            memory: "48Mi"
            cpu: "200m"
        securityContext:
          runAsUser: 1000
          runAsGroup: 1000
          allowPrivilegeEscalation: false
        ports:
          - containerPort: 8080
```

Because the Dockfile contains the `USER` command, this is essentially duplicative in terms of the `runAsUser` parameter, but it also makes it explicit who the app is supposed to run as. Also, if for some reason the `USER` command was removed or changed in the container build, this would override it, providing some protection against accidental exposure.

The `allowPrivilegeEscalation` is a directive to the underlying container runtime that prevents the container from being able to execute any privileged actions, which should be considered a default for every Deployment.

There are also mechanisms by which Kubernetes can configure AppArmor, SELinux or seccomp on the underlying host, allowing for further security risk managment. 



## K8S Best Practice: Read-only root filesystem

Containers are supposed to be immutable, so good user of containers in application design should mean that no container needs to be able to write to its root filesystem. If a container does need to write to disk, it should be a separate, specific volume that only contains a specific path.

Again, these changes happen in the Deployment file, under the `containers:` block:

```yaml
      containers:
      - name: signal-r-chat
        image: localhost:32000/signal-r-chat
        resources:
          requests:
            memory: "32Mi"
            cpu: "100m"
          limits:
            memory: "48Mi"
            cpu: "200m"
        securityContext:
          runAsUser: 1000
          runAsGroup: 1000
          allowPrivilegeEscalation: false
          readOnlyRootFilesystem: true
        ports:
          - containerPort: 8080
```

Saving and deploying this will result in a failure however, because by default, the .NET web server tries to write diagnostics to disk. This isn't something that should be happening in production anyway (at least not normally), so an `ENV` will get added to the Dockerfile to disable it:

```docker
# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app/out .
# Add a non-privileged user to run the app as
RUN useradd --home-dir /app --no-create-home \
    --no-log-init --user-group --uid 1000 signalrchat && \
    chown -R signalrchat:signalrchat ./* && \
    chmod 500 ./*
USER signalrchat
# Added to disable diagnostics writing to disk
ENV COMPlus_EnableDiagnostics=0
EXPOSE 8080
ENTRYPOINT ["dotnet", "SignalRChat.dll"]
```

If these diagnsotics were needed to be run in production (hopefully just for a limited time for debugging purposes), the ideal solution would be to mount a separate volume and then configure the diagnostic output to write there rather than the root.

## K8S Patterns: Scaling Up

It would be very simple to change the `replicas:` value in the Deployment from `1` to `3`, but that only works when an application is genuinely stateless, and the chat app isn't at this point. If the Deployment was scaled up, connections would get round-robined to each running Pod, functionally resulting in each Pod being an isolated chat room.

SignalR has can be configured to use various backends to keep track of state across multiple instances of the app, and for the purposes of this illustration, a Redis instance will be created inside the K8S cluster so that it can back the chat app and any user connected to any instance will see all of the messages.

## The Redis Operator

Redis is a complicated enough service that unless it's being used purely for ephemeral storage (with a transparent caching layer), it's not workable to manage it directly using `Deployment` or `StatefulSet` abstractions in K8S. It does however have multiple options for the `Operator` pattern, where a separate controller is deployed inside the cluster with the task of creating and managing the lifecycle of Redis resources. This is typically paired with the creation of a K8S Custom Resource Definition (CRD), so admins can simply declare a `Redis` or `RedisCluster` resource type (and the attending configuration) directly in their K8S manifests.

To start, we'll deploy the Operator itself using the K8S Helm package manager:

```bash
helm upgrade redis-operator ot-helm/redis-operator \
    --install --namespace redis-operator --create-namespace
```

As mentioned above, the Operator is a `Deployment` unto itself, a single pod that provides API paths via the standard K8S API that perform actions that might historically be done manually by a sysadmin. To validate it's running, use `kubectl` and specify the namespace it's running in:

```bash
kubectl -n redis-operator get pods
```

If things are working well, it should look something like this:

```
NAME                              READY   STATUS    RESTARTS   AGE
redis-operator-6f7dd898d9-skk94   1/1     Running   0          4m36s
```

It can also be interesting to look at the logs for initial deployments:

```bash
kubectl -n redis-operator logs <pod name from previous command>
```

Now that the operator is installed, 